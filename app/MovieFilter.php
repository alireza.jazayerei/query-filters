<?php


namespace App;


class MovieFilter extends QueryFilter
{
    public function genre($genre)
    {
        return $this->builder->where('genre', $genre);
    }

    public function year($year)
    {
        return $this->builder->where('year', ">=", $year);
    }
}
