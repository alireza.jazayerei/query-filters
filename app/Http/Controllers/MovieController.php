<?php

namespace App\Http\Controllers;

use App\Movie;
use App\MovieFilter;


class MovieController extends Controller
{
    public function index(MovieFilter $filters)
    {
        return Movie::filter($filters)->get();
    }
}
