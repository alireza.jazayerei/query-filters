<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Movie;
use Faker\Generator as Faker;

$factory->define(Movie::class, function (Faker $faker) {
    return [
        "name" =>$faker->name,
        "imdb" =>$faker->numberBetween(0,10),
        "user_rating" =>$faker->numberBetween(0,10),
        "country" =>$faker->country,
        "year" =>$faker->year(),
        "genre" =>$faker->randomElement(["action","comedy","love","drama"]),
        "quality" =>$faker->randomElement(["720","480","1080"]),
        "created_at" =>$faker->dateTime(),
    ];
});
